Exercise 1
----

Task: Setup Ubuntu 20.04 LTS on your local machine using Vagrant.
==

**Instruction:** 

Customize your Vagrantfile as necessary with private_network set to dhcp
Once the machine is up, run ifconfig and share the output in your submission along with your Vagrantfile in a folder for this exercise.


Exercise 2
-----

****Task: Research online for 10 more linux commands aside the ones already mentioned in this module. Submit using your altschool-cloud-exercises project, explaining what each command is used for with examples of how to use each and example screenshots of using each of them.****

Instruction: Submit your work in a folder for this exercise in your altschool-cloud-exercises project. You will need to learn how to embed images in markdown files.

[1.]
Shows a history of all your linux command that has been executed.

![image-1.png](./image-1.png)

[2.] sudo apt-get purge -auto remove: deletes an installed package from your linux machine.

[3.] vagrant reload: reloads your vagrant and clears any previously set ports

![image-3.png](./image-3.png)

[4.] Sudo apt-get is another command for installation

![image-4.png](./image-4.png)

[5.] sudo apt update: updates your system

![image-5.png](./image-5.png)

[6.] rm -r filename: removes a directory

![image-6.png](./image-6.png)

[7.] man: gives a detailed information on a particular command

![image-7.png](./image-7.png)

![image-8.png](./image-8.png)

[8.] ls -R: Lists files in sub-directories as well

![image-9.png](./image-9.png)

[9.] apt list installed: shows all the packages that are installed in yourlinux machine.

![image-10.png](./image-10.png)

[10.] sudo apt-get clean: cleans your machines of the leftover package that has been removed.


Exercise3:
====
***Create 3 groups – admin, support & engineering and add the admin group to sudoers. 
Create a user in each of the groups. 
Generate SSH keys for the user in the admin group
Instruction:***

****Submit the contents of /etc/passwd, /etc/group, /etc/sudoers****

Content of cat /etc/passwd

![image-11.png](./image-11.png)

Content of cat /etc/group

![image-13.png](./image-13.png)

Content of cat/etc/group

![image-12.png](./image-12.png)

Content of cat /etc/sudoers

![image-14.png](./image-14.png)
